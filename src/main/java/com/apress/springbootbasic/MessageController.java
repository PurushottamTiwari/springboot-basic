package com.apress.springbootbasic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.quartz.QuartzProperties;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;

@RestController
public class MessageController {


    @Autowired(required = false)
    SpringMessage springMessage;

    @Autowired(required = false)
    SpringBootMessage springBootMessage;


    @Autowired(required = false)
    DataSource dataSource;

    @GetMapping(path = "/message")
    public String getMessage() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);

        Employee employee = jdbcTemplate.queryForObject("select * from employee", BeanPropertyRowMapper.newInstance(Employee.class));
        return  "Hello" + employee.getName() + springBootMessage.getMessage();
    }
}
