package com.apress.springbootbasic;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;

@Configuration
public class AppConfig {
    @Bean
    @Conditional(StringMessageCondition.class)
    public SpringMessage getSpringMessage() {
        return new SpringMessage();
    }

    @Bean
    @Conditional(SpringBootMessageCondition.class)
    public SpringBootMessage getSpringBootMessage() {
        return new SpringBootMessage();
    }

    @Bean
    @Conditional(OracleCheck.class)
    public DataSource  getOracleDataSource(){
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("oracle.jdbc.OracleDriver");
        dataSourceBuilder.url("jdbc:oracle:thin:@localhost:1521:orcl");
        dataSourceBuilder.username("system");
        dataSourceBuilder.password("India#2013");
        return dataSourceBuilder.build();

    }

    /*@Bean
    @Conditional(MysqlCheck.class)
    public DataSource  getMysqlDataSource(){
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
        dataSourceBuilder.url("jdbc:mysql://localhost:3306/mysql");
        dataSourceBuilder.username("root");
        dataSourceBuilder.password("India#2013");
        return dataSourceBuilder.build();

    }*/


}