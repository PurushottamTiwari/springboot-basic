package com.apress.springbootbasic;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class OracleCheck implements Condition {



    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        String value = conditionContext.getEnvironment().getProperty("dbtype");
       // String value = System.getProperty("dbtype");
        return "oracle".equalsIgnoreCase(value);
    }
}
