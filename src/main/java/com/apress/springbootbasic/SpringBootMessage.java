package com.apress.springbootbasic;

public class SpringBootMessage implements Message {
    @Override
    public String getMessage() {
        return "Welcome in the world of SpringBoot";
    }
}
