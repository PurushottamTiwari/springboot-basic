package com.apress.springbootbasic;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class DataSourceCondition  implements Condition {
    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {

        try {
            Class<?> cls  = Class.forName("oracle.jdbc.driver.OracleDriver");
             return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }
}
