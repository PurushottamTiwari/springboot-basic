package com.apress.springbootbasic;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class StringMessageCondition implements Condition {
    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        String message = conditionContext.getEnvironment().getProperty("welcome.messagetype");
        return ("spring".equals(message));
    }
}
